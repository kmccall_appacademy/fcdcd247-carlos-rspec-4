class Dictionary
  def initialize
    @d = {}
  end

  def add(item)
    @d = @d.merge(if item.is_a?(Hash) then item else {item => nil} end)
  end

  def entries
    @d
  end

  def keywords
    @d.keys.sort
  end

  def include?(item)
    @d.keys.include?(item)
  end

  def find(str)
    resulthash = {}
    self.keywords.each do |key|
      if key.start_with?(str)
        resulthash[key] = @d[key]
      end
    end
    resulthash
  end

  def printable
    resultstr = ''
    keywords.each do |key|
      resultstr += '[%s] "%s"' % [key, @d[key]] + "\n"
    end
    resultstr[0..-2]
  end
end
