class Temperature
  def ftoc(degf)
    (degf.to_f - 32.0) * 5.0 / 9.0
  end

  def ctof(degc)
    (degc.to_f * 9.0 / 5.0) + 32.0
  end

  def initialize(opts={})
    if opts.key?(:f)
      @f = opts[:f]
      @c = ftoc(@f)
    end
    if opts.key?(:c)
      @c = opts[:c]
      @f = ctof(@c)
    end
  end

  def in_fahrenheit
    @f
  end

  def in_celsius
    @c
  end

  def self.from_fahrenheit(degf)
    Temperature.new(:f => degf)
  end

  def self.from_celsius(degc)
    Temperature.new(:c => degc)
  end
end

class Fahrenheit < Temperature
  def initialize(degf)
    super(:f => degf)
  end
end

class Celsius < Temperature
  def initialize(degc)
    super(:c => degc)
  end
end
