class Book
  def titleize(str)
    words = str.split(" ")
    result = words[0].capitalize
    words.drop(1).each do |word|
      if ["the", "and", "over", "an", "a", "in", "of"].include?(word)
        result += " " + word.downcase
      else
        result += " " + word.capitalize
      end
    end
    result
  end
  def title
    @title
  end
  def title=(val)
    @title = titleize(val)
  end
end
